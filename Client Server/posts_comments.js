export async function f (num) {
    let url1 = `https://jsonplaceholder.typicode.com/posts?userId=${num}`
    let url2 = []
    let j = 0
    for (let i = 10*num-9; i<=num*10; i++){
        url2[j]=`https://jsonplaceholder.typicode.com/comments?postId=${i}`
        j++
    }
    //console.log(url2, url1)

    let response1 = await fetch(url1)
    let response2 = []
    for(let i=0; i<10; i++){
        response2[i]=await fetch(url2[i])
    }
    console.log(response2)

    let posts = await response1.json()
    let comments = []
    for(let i=0; i<10; i++){
        comments[i]=await response2[i].json()
    }
    console.log(comments)

    let div = document.createElement('div');
    div.innerHTML = `<h2>User ${num} posts</h2>`;
    post.appendChild(div);
    for (let i = 0; i < posts.length; i++) {
        let a = (comments[i])
        console.log(a)
        let div = document.createElement("div");
        div.id = `${posts[i].id}`
        div.className = 'posts'
        div.innerHTML = `<p class="title">title: ${posts[i].title}</p><br>
                         <p class="body">posts: ${posts[i].body}</p>`
        post.appendChild(div);
        for (let j = 0; j < 5; j++) {
            let div = document.createElement("div");
            div.className='comment'
            div.innerHTML = `<h4>${a[j].id} comment</h4> <p class="cname">name: ${a[j].name}</p><br>
                             <p class="cemail">email: ${a[j].email}</p><br>
                             <p class="cbody">posts: ${a[j].body}</p>`
            post.appendChild(div);
        }
    }
}


document.getElementById('dropdown_list').onchange = function(){
    f(this.value);
}