function wrapper_dropdown() {
    let urls = [];
    for (let i = 0; i < 10; i++) {
        let m = i + 1;
        urls[i] = `https://jsonplaceholder.typicode.com/users/${m}`
    }
    let requests =  urls.map(url =>  fetch(url));
    Promise.all(requests)
        .then(dropdown_list => Promise.all(dropdown_list.map(r => r.json())))
        .then(users => {
            for (let i = 0; i < users.length; i++) {
                let list = document.createElement("option");
                list.className='user'
                list.value = i+1
                list.innerHTML = `${users[i].id} ${users[i].name}`
                dropdown_list.appendChild(list);
            }
        })
}
wrapper_dropdown()
